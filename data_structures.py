movies = ['The Intouchables', 'Cast Away', 'Love', 'The Shawshank ', 'Green Book']

last_movie = movies[-1]
movies.append('LOTR')
movies.append('Titanic')
print(len(movies))
middle_movies = movies[2:6]
print(middle_movies)

movies.insert(0, 'Top Gun 2')
print(movies)

emails = ['a@example.com', 'b@exapmle.com']
print(len(emails))
print(emails[0])
print(emails[-1])
emails.append('cde@example.com')

friend = {
    "name": "Natalia",
    "age": "32",
    "hobby": ["reading", "cooking"]

}

friend_hobbies = friend["hobby"]
print("Hobbies of my friend:", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies")
friend["hobby"].append("football")
print(friend)
