import string
import random


# Function returning the sum of the numbers from the list
def calculate_average_of_list_numbers(input_list):
    return sum(input_list) / len(input_list)


# Function returning the avearge of two numbers
def calculate_average_of_two_numbers(a, b):
    return (a + b) / 2


# Get password pf length 8 with letters, digits and symbols
def generate_random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(characters) for i in range(8))


# Function generating random login data
def generate_login_data(email_adress):
    random_password = generate_random_password()
    return {"email": email_adress, "password": random_password}


#
def calculate_player_level(exp_points):
    return exp_points // 1000


def describe_player(player_dictionary):
    nick = player_dictionary['nick']
    type = player_dictionary['type']
    exp_points = player_dictionary['exp_points']
    level = calculate_player_level(exp_points)
    print(f'The player "{nick}" is of type {type.capitaize} and has {exp_points} EXP. The player is on level {level}'),


if __name__ == '__main__':
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]
    january_average = calculate_average_of_list_numbers(january)
    february_average = calculate_average_of_list_numbers(february)
    bimonthly_average = calculate_average_of_two_numbers(january_average, february_average)
    print(bimonthly_average)

print(generate_login_data("kasia@example.com"))
print(generate_login_data("basia@example.com"))
print(generate_login_data("adam@example.com"))

player1 = {
    'nick': 'maestro_54',
    'type': 'warrior',
    'exp_points': 3000,
}
player = {
    'nick': 'asdf',
    'type': 'mag'
            'exp_points': 1000
}
describe_player(player1)
describe_player(player2)
