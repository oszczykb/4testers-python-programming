def calculate_square(number):
    return number ** 2


def celsius_to_fahrenheit(temperature_celsius):
    return temperature_celsius * 1.8 + 32


def calculate_volume_of_a_cuboid(a, b, c):
    return a * b * c


if __name__ == '__main__':
    # Zadanie 1
    print(calculate_square(0))
    print(calculate_square(16))
    print(calculate_square(2.55))

    # Zadanie 2
    print(celsius_to_fahrenheit(20))

    # Zadanie 3
    print(calculate_volume_of_a_cuboid(3, 5, 7))
