first_name = "Bogumiła"
last_name = "Oszczyk"
age = 33

print(first_name)
print(last_name)
print(age)


friend_name = "Natalia"
friend_age = 32
friend_number_of_animals = 0
friend_has_a_driving_license = True
friendship_duration_in_years = 17.5

print(friend_name, friend_age, friend_number_of_animals, friend_has_a_driving_license, friendship_duration_in_years, sep=',')

