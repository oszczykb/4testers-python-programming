def display_speed_information(speed):
    # if speed <= 50:
    # print('Thank you, your speed is below limit! :)')
    # else:
    # print('Slow down! :(')
    if speed > 50:
        print('Slow down! :(')
    else:
        print('Thank you, your speed is below limit! :)')


def check_normal_conditions(temperature, pressure):
    if temperature == 0 and pressure == 1013:
        return True
    else:
        False


def calculate_fine_amount(speed):
    return 500 + (speed - 50) * 10


def print_the_value_of_speeding_fine_in_built_up_area(speed):
    print('Your speed was:', speed)
    if speed > 100:
        print('You just lost your driving license :(')
    elif speed > 50:
        fine_amount = 500 + (speed - 50) * 10
        print('You just got a fine! :(')
    else:
        print("Thank you, your speed is fine")


def get_grade_info(grade):
    # if not (isinstance(grade, float) or isinstance(grade, int)):
    # return 'N/A'
    if grade < 2 or grade > 5:
        return 'N/A'
    elif grade >= 4.5:
        return 'bardzo dobry'
    elif grade >= 4.0:
        return 'dobry'
    elif grade >= 3.0:
        return 'dostateczny'
    else:
        return 'niedostateczny'


def get_list_of_temperature_from_celsius_to_farenheit(temps_celsius):
    temps_farenheit = []
    for temp in temps_celsius:
        temp_f = temp * 9 / 5 + 32
        temps_farenheit.append(temp_f)
    return temps_farenheit


if __name__ == '__main__':
    display_speed_information(50)
    display_speed_information(49)
    display_speed_information(51)

    print(check_normal_conditions(0, 1013))
    print(check_normal_conditions(1, 1013))
    print(check_normal_conditions(0, 1014))
    print(check_normal_conditions(1, 1014))

    print_the_value_of_speeding_fine_in_built_up_area(101)
    print_the_value_of_speeding_fine_in_built_up_area(100)
    print_the_value_of_speeding_fine_in_built_up_area(49)
    print_the_value_of_speeding_fine_in_built_up_area(50)
    print_the_value_of_speeding_fine_in_built_up_area(51)

    print(get_grade_info(5))
    print(get_grade_info(4.5))
    print(get_grade_info(4))
    print(get_grade_info(3))
    print(get_grade_info(2))
    print(get_grade_info(1))
    print(get_grade_info(6))

    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print(get_list_of_temperature_from_celsius_to_farenheit(temps_celsius))
