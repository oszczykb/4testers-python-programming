import random
import time
import uuid


def generation_random_number():
    return random.randint(0, 40)


def generation_random_email():
    domain = 'example.com'
    names_list = ['james', 'kate', 'jack', 'mark', 'bill', 'veronika', 'susan']
    random_name = random.choice(names_list)
    # suffix = random.randit(1, 1_000_000)
    suffix = str(uuid.uuid4())
    return f'{random_name}.{suffix}@{random}'


def generation_random_boolean():
    return random.choice([True, False])


def get_dictionary_with_random_personale_date():
    random_email = generation_random_email()
    random_number = generation_random_number()
    random_boolean = generation_random_boolean()
    return {
        "email": random_email,
        "seniority_years": random_number,
        "female": random_email
    }


def generation_list_of_dictionaries_with_random_personal_date(number_of_dictionaries):
    list_of_dictionaries = []
    for number in range(number_of_dictionaries):
        list_of_dictionaries.append(get_dictionary_with_random_personale_date())
    return list_of_dictionaries


def get_emails_of_workers_with_seniority_years_above_10(list_of_employees):
    senior_employees = []
    for employee in list_of_employees:
        if employee['seniority_years'] > 10:
            senior_employees.append(employee['email'])
    return senior_employees


def get_emails_of_workers_with_seniority_years_above(list_of_employees, seniority_years_margin):
    senior_employees = []
    for employee in list_of_employees:
        if employee['seniority_years'] > seniority_years_margin:
            senior_employees.append(employee['email'])
    return senior_employees


def get_list_of_female_employees(list_of_employees):
    filtered_employees = []
    for employee in list_of_employees:
        if employee["female"] == True:
            filtered_employees.append(employee)
    return filtered_employees


def get_list_of_employees_filtered_by_gender(list_of_employees, is_female):
    filtered_employees = []
    for employee in list_of_employees:
        if employee["female"] == is_female:
            filtered_employees.append(employee)
    return filtered_employees


if __name__ == '__main__':
    print(get_dictionary_with_random_personale_date())
    print(generation_list_of_dictionaries_with_random_personal_date(10))
    test_employee_list = generation_list_of_dictionaries_with_random_personal_date(20)
    print(test_employee_list)
    test_senior_employee_emails = get_emails_of_workers_with_seniority_years_above(test_employee_list, 10)
    print(test_senior_employee_emails)
    test_female_employees = get_list_of_female_employees(test_employee_list)
    print(test_female_employees)
    test_male_employees = get_list_of_employees_filtered_by_gender(test_employee_list, False)
    print(test_male_employees)
    print(len(test_employee_list))
    print(len(test_female_employees))
    print(len(test_male_employees))
