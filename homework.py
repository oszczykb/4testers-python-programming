import random
import datetime

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def generate_email_address(person_first_name, person_last_name):
    print(f"{person_first_name.lower()}.{person_last_name.lower()}@example.com")


def is_adult(age):
    if age >= 18:
        return True
    else:
        return False


def generate_last_name():
    return random.choice(female_fnames)


def generate_person(name_list, person_dict_list):
    name = random.choice(name_list)
    lastname = random.choice(surnames)
    age = random.randint(5, 45)

    person_dict_list.append({
        "firstname": name,
        "lastname": lastname,
        "country": random.choice(countries),
        "email": f"{name.lower()}.{lastname.lower()}@example.com",
        "age": age,
        "adult": is_adult(age),
        "birth_year": datetime.date.today().year - age
    })


def introduce(person_dict):
    print(
        f"Hi! I'm {person_dict['firstname']} {person_dict['lastname']}. I come from {person_dict['country']} and I was born in {person_dict['birth_year']}.")


def generate_persons():
    person_dicts = []
    for i in range(0, 5):
        generate_person(female_fnames, person_dicts)

    for i in range(0, 5):
        generate_person(male_fnames, person_dicts)

    return person_dicts


if __name__ == '__main__':
    generated_person_dict_list = generate_persons()

    for person in generated_person_dict_list:
        introduce(person)
