def print_greetings_for_a_person_in_the_city(person_name, city):
    print(f"Witaj {person_name}! Miło Cie widzieć w naszym mieście: {city}!")


def generate_email_address(person_first_name, person_last_name):
    print(person_first_name.lower()}.{person_last_name.lower()}@4testers.pl")


if __name__ == '__main__':
    # Zadanie 1
    print_greetings_for_a_person_in_the_city("Kasia", "Szczeciń")
    print_greetings_for_a_person_in_the_city("Adam", "Poznań")

    # Zadanie 2
    generate_email_address("Janusz", "Nowak")
    generate_email_address("Barbara", "Kowalska")
