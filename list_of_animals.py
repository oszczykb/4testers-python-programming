animals = [
    {'name': 'Burek', 'kind': 'dog', 'age': 7},
    {'name': 'Bonifacy', 'kind': 'cat', 'age': None},
    {'name': 'Misio', 'kind': 'hamster', 'age': 1}

]
print(animals[-1]['name'])
animals[1]['age'] = 2
print(animals[1])
animals.insert(0, {'name': 'Reksio', 'kind': 'dog', 'age': 9})
print(animals)

adressess = [
    {'city': 'Warszawa', 'street': 'Grochowska', 'house_number': '304', 'post_code': '03-840'},
    {'city': 'Kraków', 'street': 'Szeroka', 'house_number': '5', 'post_code': '31-053'},
    {'city': 'Wrocław', 'street': 'Piękna', 'house_number': '27', 'post_code': '50-506'},
]

if __name__ == '__main__':
    print(adressess[2]['post_code'])
    print((adressess[1], ['city']))
    adressess[0]['city'] = 'Poznań'
    print(adressess)
